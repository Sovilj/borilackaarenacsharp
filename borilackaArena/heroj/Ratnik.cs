﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace borilackaArena.heroj
{
    public class Ratnik : Heroj
    {

    public Ratnik()
    {
        this.setNazivHeroja("Ratnik");
        this.setKriticnaSansa(0);
        this.setMaxSnaga(100);
        this.setMaxZdravlje(100);
        this.setTrenutniNivo(1);
        this.setTrenutnoIskustvo(0);
        this.setTrenutnoZdravlje(100);
        this.setTrenutnaSnaga(100);
        this.setMaxIskustvo(50);
        this.setMinSteta(2);
        this.setMaxSteta(6);
    }
}


}
