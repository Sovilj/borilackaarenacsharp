﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace borilackaArena.carolija
{
    public abstract class Carolija
    {

        private string nazivCarolije;
        private int maxNanesenaSteta;
        private int minNanesenaSteta;

        public string getNazivCarolije()
        {
            return nazivCarolije;
        }

        public void setNazivCarolije(string nazivCarolije)
        {
            this.nazivCarolije = nazivCarolije;
        }

        public int getMaxNanesenaSteta()
        {
            return maxNanesenaSteta;
        }

        public void setMaxNanesenaSteta(int maxNanesenaSteta)
        {
            this.maxNanesenaSteta = maxNanesenaSteta;
        }

        public int getMinNanesenaSteta()
        {
            return minNanesenaSteta;
        }

        public void setMinNanesenaSteta(int minNanesenaSteta)
        {
            this.minNanesenaSteta = minNanesenaSteta;
        }

    }


}
