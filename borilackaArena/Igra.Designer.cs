﻿namespace borilackaArena
{
    partial class Igra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.borbenaArena = new System.Windows.Forms.TextBox();
            this.vrstaBorbe = new System.Windows.Forms.TextBox();
            this.igrac1 = new System.Windows.Forms.TextBox();
            this.igrac2 = new System.Windows.Forms.TextBox();
            this.nivoHeroj1 = new System.Windows.Forms.TextBox();
            this.iskustvoHeroj1 = new System.Windows.Forms.TextBox();
            this.zdravljeHeroj1 = new System.Windows.Forms.TextBox();
            this.snagaHeroj1 = new System.Windows.Forms.TextBox();
            this.carolija1Heroj1 = new System.Windows.Forms.TextBox();
            this.nivoHeroj2 = new System.Windows.Forms.TextBox();
            this.iskustvoHeroj2 = new System.Windows.Forms.TextBox();
            this.zdravljeHeroj2 = new System.Windows.Forms.TextBox();
            this.snagaHeroj2 = new System.Windows.Forms.TextBox();
            this.carolija1Heroj2 = new System.Windows.Forms.TextBox();
            this.carolija2Heroj1 = new System.Windows.Forms.TextBox();
            this.carolija3Heroj1 = new System.Windows.Forms.TextBox();
            this.carolija2Heroj2 = new System.Windows.Forms.TextBox();
            this.carolija3Heroj2 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.osveziti = new System.Windows.Forms.Button();
            this.izabranaCarolija1 = new System.Windows.Forms.TextBox();
            this.izabranaCarolija2 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(711, 388);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "KRAJ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.sledecaStrana);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "BORBA";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(267, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Arena";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(551, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Način borbe";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(122, 132);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Iskustvo";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(122, 107);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Nivo";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(122, 159);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Zdravlje";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(122, 184);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Snaga";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.label8.Location = new System.Drawing.Point(51, 236);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 25);
            this.label8.TabIndex = 8;
            this.label8.Text = "Čarolije";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(208, 62);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "HEROJ 1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(332, 62);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "HEROJ 2";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 17.25F);
            this.label11.Location = new System.Drawing.Point(617, 78);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(128, 29);
            this.label11.TabIndex = 11;
            this.label11.Text = "ČAROLIJE";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.label12.Location = new System.Drawing.Point(599, 120);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(187, 25);
            this.label12.TabIndex = 12;
            this.label12.Text = "1. Osnovni udarac";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.label13.Location = new System.Drawing.Point(599, 147);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(183, 25);
            this.label13.TabIndex = 13;
            this.label13.Text = "2. Magični udarac";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.label14.Location = new System.Drawing.Point(599, 172);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(174, 25);
            this.label14.TabIndex = 14;
            this.label14.Text = "3. Snažni udarac";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.label15.Location = new System.Drawing.Point(599, 197);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(140, 25);
            this.label15.TabIndex = 15;
            this.label15.Text = "4. Brz udarac";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.label16.Location = new System.Drawing.Point(599, 222);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(129, 25);
            this.label16.TabIndex = 16;
            this.label16.Text = "5. Oporavak";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(40, 373);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(99, 13);
            this.label17.TabIndex = 17;
            this.label17.Text = "Čarolija pod brojem:";
            // 
            // borbenaArena
            // 
            this.borbenaArena.Location = new System.Drawing.Point(308, 23);
            this.borbenaArena.Name = "borbenaArena";
            this.borbenaArena.Size = new System.Drawing.Size(100, 20);
            this.borbenaArena.TabIndex = 18;
            // 
            // vrstaBorbe
            // 
            this.vrstaBorbe.Location = new System.Drawing.Point(622, 23);
            this.vrstaBorbe.Name = "vrstaBorbe";
            this.vrstaBorbe.Size = new System.Drawing.Size(100, 20);
            this.vrstaBorbe.TabIndex = 19;
            // 
            // igrac1
            // 
            this.igrac1.Location = new System.Drawing.Point(183, 78);
            this.igrac1.Name = "igrac1";
            this.igrac1.Size = new System.Drawing.Size(100, 20);
            this.igrac1.TabIndex = 20;
            // 
            // igrac2
            // 
            this.igrac2.Location = new System.Drawing.Point(308, 78);
            this.igrac2.Name = "igrac2";
            this.igrac2.Size = new System.Drawing.Size(100, 20);
            this.igrac2.TabIndex = 21;
            // 
            // nivoHeroj1
            // 
            this.nivoHeroj1.Location = new System.Drawing.Point(183, 103);
            this.nivoHeroj1.Name = "nivoHeroj1";
            this.nivoHeroj1.Size = new System.Drawing.Size(100, 20);
            this.nivoHeroj1.TabIndex = 22;
            // 
            // iskustvoHeroj1
            // 
            this.iskustvoHeroj1.Location = new System.Drawing.Point(183, 129);
            this.iskustvoHeroj1.Name = "iskustvoHeroj1";
            this.iskustvoHeroj1.Size = new System.Drawing.Size(100, 20);
            this.iskustvoHeroj1.TabIndex = 23;
            // 
            // zdravljeHeroj1
            // 
            this.zdravljeHeroj1.Location = new System.Drawing.Point(183, 155);
            this.zdravljeHeroj1.Name = "zdravljeHeroj1";
            this.zdravljeHeroj1.Size = new System.Drawing.Size(100, 20);
            this.zdravljeHeroj1.TabIndex = 24;
            // 
            // snagaHeroj1
            // 
            this.snagaHeroj1.Location = new System.Drawing.Point(183, 181);
            this.snagaHeroj1.Name = "snagaHeroj1";
            this.snagaHeroj1.Size = new System.Drawing.Size(100, 20);
            this.snagaHeroj1.TabIndex = 25;
            // 
            // carolija1Heroj1
            // 
            this.carolija1Heroj1.Location = new System.Drawing.Point(183, 207);
            this.carolija1Heroj1.Name = "carolija1Heroj1";
            this.carolija1Heroj1.Size = new System.Drawing.Size(100, 20);
            this.carolija1Heroj1.TabIndex = 26;
            // 
            // nivoHeroj2
            // 
            this.nivoHeroj2.Location = new System.Drawing.Point(308, 104);
            this.nivoHeroj2.Name = "nivoHeroj2";
            this.nivoHeroj2.Size = new System.Drawing.Size(100, 20);
            this.nivoHeroj2.TabIndex = 27;
            // 
            // iskustvoHeroj2
            // 
            this.iskustvoHeroj2.Location = new System.Drawing.Point(308, 130);
            this.iskustvoHeroj2.Name = "iskustvoHeroj2";
            this.iskustvoHeroj2.Size = new System.Drawing.Size(100, 20);
            this.iskustvoHeroj2.TabIndex = 28;
            // 
            // zdravljeHeroj2
            // 
            this.zdravljeHeroj2.Location = new System.Drawing.Point(308, 156);
            this.zdravljeHeroj2.Name = "zdravljeHeroj2";
            this.zdravljeHeroj2.Size = new System.Drawing.Size(100, 20);
            this.zdravljeHeroj2.TabIndex = 29;
            // 
            // snagaHeroj2
            // 
            this.snagaHeroj2.Location = new System.Drawing.Point(308, 181);
            this.snagaHeroj2.Name = "snagaHeroj2";
            this.snagaHeroj2.Size = new System.Drawing.Size(100, 20);
            this.snagaHeroj2.TabIndex = 30;
            // 
            // carolija1Heroj2
            // 
            this.carolija1Heroj2.Location = new System.Drawing.Point(308, 209);
            this.carolija1Heroj2.Name = "carolija1Heroj2";
            this.carolija1Heroj2.Size = new System.Drawing.Size(100, 20);
            this.carolija1Heroj2.TabIndex = 31;
            // 
            // carolija2Heroj1
            // 
            this.carolija2Heroj1.Location = new System.Drawing.Point(183, 242);
            this.carolija2Heroj1.Name = "carolija2Heroj1";
            this.carolija2Heroj1.Size = new System.Drawing.Size(100, 20);
            this.carolija2Heroj1.TabIndex = 32;
            // 
            // carolija3Heroj1
            // 
            this.carolija3Heroj1.Location = new System.Drawing.Point(183, 277);
            this.carolija3Heroj1.Name = "carolija3Heroj1";
            this.carolija3Heroj1.Size = new System.Drawing.Size(100, 20);
            this.carolija3Heroj1.TabIndex = 33;
            // 
            // carolija2Heroj2
            // 
            this.carolija2Heroj2.Location = new System.Drawing.Point(308, 242);
            this.carolija2Heroj2.Name = "carolija2Heroj2";
            this.carolija2Heroj2.Size = new System.Drawing.Size(100, 20);
            this.carolija2Heroj2.TabIndex = 34;
            // 
            // carolija3Heroj2
            // 
            this.carolija3Heroj2.Location = new System.Drawing.Point(308, 277);
            this.carolija3Heroj2.Name = "carolija3Heroj2";
            this.carolija3Heroj2.Size = new System.Drawing.Size(100, 20);
            this.carolija3Heroj2.TabIndex = 35;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(17, 62);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 36;
            this.button2.Text = "POČETAK";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.zapocni);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(251, 369);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 37;
            this.button3.Text = "Započni igru";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.zapocniIgru);
            // 
            // osveziti
            // 
            this.osveziti.Location = new System.Drawing.Point(647, 295);
            this.osveziti.Name = "osveziti";
            this.osveziti.Size = new System.Drawing.Size(75, 23);
            this.osveziti.TabIndex = 38;
            this.osveziti.Text = "Osveži";
            this.osveziti.UseVisualStyleBackColor = true;
            this.osveziti.Click += new System.EventHandler(this.trenutanRezultat);
            // 
            // izabranaCarolija1
            // 
            this.izabranaCarolija1.Location = new System.Drawing.Point(145, 371);
            this.izabranaCarolija1.Name = "izabranaCarolija1";
            this.izabranaCarolija1.Size = new System.Drawing.Size(100, 20);
            this.izabranaCarolija1.TabIndex = 39;
            // 
            // izabranaCarolija2
            // 
            this.izabranaCarolija2.Location = new System.Drawing.Point(332, 371);
            this.izabranaCarolija2.Name = "izabranaCarolija2";
            this.izabranaCarolija2.Size = new System.Drawing.Size(100, 20);
            this.izabranaCarolija2.TabIndex = 40;
            // 
            // Igra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.izabranaCarolija2);
            this.Controls.Add(this.izabranaCarolija1);
            this.Controls.Add(this.osveziti);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.carolija3Heroj2);
            this.Controls.Add(this.carolija2Heroj2);
            this.Controls.Add(this.carolija3Heroj1);
            this.Controls.Add(this.carolija2Heroj1);
            this.Controls.Add(this.carolija1Heroj2);
            this.Controls.Add(this.snagaHeroj2);
            this.Controls.Add(this.zdravljeHeroj2);
            this.Controls.Add(this.iskustvoHeroj2);
            this.Controls.Add(this.nivoHeroj2);
            this.Controls.Add(this.carolija1Heroj1);
            this.Controls.Add(this.snagaHeroj1);
            this.Controls.Add(this.zdravljeHeroj1);
            this.Controls.Add(this.iskustvoHeroj1);
            this.Controls.Add(this.nivoHeroj1);
            this.Controls.Add(this.igrac2);
            this.Controls.Add(this.igrac1);
            this.Controls.Add(this.vrstaBorbe);
            this.Controls.Add(this.borbenaArena);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Name = "Igra";
            this.Text = "Igra";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox borbenaArena; 
        private System.Windows.Forms.TextBox vrstaBorbe; 
        private System.Windows.Forms.TextBox igrac1; 
        private System.Windows.Forms.TextBox igrac2; 
        private System.Windows.Forms.TextBox nivoHeroj1; 
        private System.Windows.Forms.TextBox iskustvoHeroj1; 
        private System.Windows.Forms.TextBox zdravljeHeroj1; 
        private System.Windows.Forms.TextBox snagaHeroj1; 
        private System.Windows.Forms.TextBox carolija1Heroj1; 
        private System.Windows.Forms.TextBox nivoHeroj2; 
        private System.Windows.Forms.TextBox iskustvoHeroj2; 
        private System.Windows.Forms.TextBox zdravljeHeroj2; 
        private System.Windows.Forms.TextBox snagaHeroj2; 
        private System.Windows.Forms.TextBox carolija1Heroj2; 
        private System.Windows.Forms.TextBox carolija2Heroj1; 
        private System.Windows.Forms.TextBox carolija3Heroj1; 
        private System.Windows.Forms.TextBox carolija2Heroj2; 
        private System.Windows.Forms.TextBox carolija3Heroj2; 
        private System.Windows.Forms.Button button2; 
        private System.Windows.Forms.Button button3; 
        private System.Windows.Forms.Button osveziti;
        private System.Windows.Forms.TextBox izabranaCarolija1; 
        private System.Windows.Forms.TextBox izabranaCarolija2; 
    }
}