﻿using borilackaArena.arena;
using borilackaArena.carolija;
using borilackaArena.heroj;
using borilackaArena.nacinborbe;
using System;
using System.Threading;

namespace borilackaArena.borba
{
    public class Borba
    {
        public static string nivo1, nivo2, snaga1, snaga2, zdravlje1, zdravlje2, iskustvo1, iskustvo2, eksponat11, eksponat12, eksponat13, eksponat21, eksponat22, eksponat23;
        public Random randomIzabranBroj = new Random();
        public arena.Arena osnovnaArena = new OsnovnaArena(), arenaUbica = new ArenaUbica(), ledenaArena = new LedenaArena();
        public Carolija osnovniUdarac = new OsnovniUdarac(), magicniUdarac = new MagicniUdarac(), snazniUdarac = new SnazniUdarac(), brzUdarac = new BrzUdarac();
        public nacinborbe.NacinBorbe epicentar = new Epicentar(), osnovni = new Osnovni();
        public heroj.Heroj carobnjak = new Carobnjak(), svestenik = new Svestenik(), ratnik = new Ratnik();
        private int arenaZaIgrace = 0;
        public int nacinBorbeBoraca = 0;
        private string protivnik1, protivnik2;
         public static Semaphore prviBorac = new Semaphore(0,1), drugiBorac = new Semaphore(1,1);

        public heroj.Heroj getCarobnjak()
        {
            return carobnjak;
        }

        public heroj.Heroj getSvestenik()
        {
            return svestenik;
        }

        public heroj.Heroj getRatnik()
        {
            return ratnik;
        }

        public void proveraHeroja1(heroj.Heroj heroj, int carolija)
        {
            if (protivnik2 == carobnjak.getNazivHeroja())
            {
                proveraKriticneSanseHeroj1(heroj, carobnjak, carolija);
            }
            else if (protivnik2 == svestenik.getNazivHeroja())
            {
                proveraKriticneSanseHeroj1(heroj, svestenik, carolija);
            }
            else if (protivnik2 == ratnik.getNazivHeroja())
            {
                proveraKriticneSanseHeroj1(heroj, ratnik, carolija);
            }
        }


        /**
         * Ova metoda poziva metodu za stampanje rezultata
         */
        public void prikazRezultata()
        {
            if (protivnik1 == carobnjak.getNazivHeroja())
            {
                carolije(carobnjak);
                if (protivnik2 == svestenik.getNazivHeroja())
                {
                    carolije(svestenik);
                    stampanje(carobnjak, svestenik);
                }
                else
                {
                    carolije(ratnik);
                    stampanje(carobnjak, ratnik);
                }
            }
            else if (protivnik1 == svestenik.getNazivHeroja())
            {
                carolije(svestenik);
                if (protivnik2 == carobnjak.getNazivHeroja())
                {
                    carolije(carobnjak);
                    stampanje(svestenik, carobnjak);
                }
                else
                {
                    carolije(ratnik);
                    stampanje(svestenik, ratnik);
                }
            }
            else
            {
                carolije(ratnik);
                if (protivnik2 == carobnjak.getNazivHeroja())
                {
                    carolije(carobnjak);
                    stampanje(ratnik, carobnjak);
                }
                else
                {
                    carolije(svestenik);
                    stampanje(ratnik, svestenik);
                }
            }
        }

        /**
        * Ova metoda vraca nacin borbe koju je igrac izabrao
        *
        * @return (epicentar ili standardi)
        */
        public string nacinBorbeUAreni()
        {

            string zeljeniNacinBorbeBoraca = NacinBorbe.odabraniNacinBorbe;
            switch (zeljeniNacinBorbeBoraca)
            {
                case "1":
                    nacinBorbeBoraca = 1;
                    return osnovni.getNacinBorbe();
                case "2":
                    nacinBorbeBoraca = 2;
                    return epicentar.getNacinBorbe();
                default:
                    nacinBorbeBoraca = 1;
                    return osnovni.getNacinBorbe();
            }
        }

        /**
       * Ova metoda proverava kriticnu sansu i ispravlja trenutno zdravlje heroja
       *
       * @param heroj1
       * @param heroj2
       */
        public void proveraKriticneSanseHeroj1(heroj.Heroj heroj1, heroj.Heroj heroj2, int carolija)
        {

            if (carolija == 1)
            {
                if (heroj2.getTrenutnoZdravlje() >= osnovniUdarac.getMinNanesenaSteta())
                {
                    heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() - osnovniUdarac.getMinNanesenaSteta());
                }
                else if (heroj2.getTrenutnoZdravlje() >= osnovniUdarac.getMaxNanesenaSteta())
                {
                    heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() - osnovniUdarac.getMaxNanesenaSteta());
                }
            }
            else if (carolija == 2)
            {
                if (heroj2.getTrenutnoZdravlje() >= magicniUdarac.getMinNanesenaSteta())
                {
                    heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() - magicniUdarac.getMinNanesenaSteta());
                }
                else if (heroj2.getTrenutnoZdravlje() >= magicniUdarac.getMaxNanesenaSteta())
                {
                    heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() - magicniUdarac.getMaxNanesenaSteta());
                }
            }
            else if (carolija == 3)
            {
                if (heroj2.getTrenutnoZdravlje() >= snazniUdarac.getMinNanesenaSteta())
                {
                    heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() - snazniUdarac.getMinNanesenaSteta());
                }
                else if (heroj2.getTrenutnoZdravlje() >= snazniUdarac.getMaxNanesenaSteta())
                {
                    heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() - snazniUdarac.getMaxNanesenaSteta());
                }
            }
            else if (carolija == 4)
            {
                if (heroj2.getTrenutnoZdravlje() >= brzUdarac.getMinNanesenaSteta())
                {
                    heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() - brzUdarac.getMinNanesenaSteta());
                }
                else if (heroj2.getTrenutnoZdravlje() >= brzUdarac.getMaxNanesenaSteta())
                {
                    heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() - brzUdarac.getMaxNanesenaSteta());
                }
            }
            if (heroj2.getTrenutnoZdravlje() >= heroj2.getMaxSteta())
            {
                heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() - heroj2.getMaxSteta());

            }
            else if (heroj2.getTrenutnoZdravlje() >= heroj2.getMinSteta())
            {
                heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() - heroj2.getMinSteta());
            }
            heroj1.setTrenutnoIskustvo(heroj1.getTrenutnoIskustvo() + 4);
            nivo(heroj1);
            if (heroj1.getTrenutniNivo() == 2 || heroj1.getTrenutniNivo() == 3)
            {
                heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() + 7);
            }
            if (heroj1.getTrenutniNivo() == 4 || heroj1.getTrenutniNivo() == 5)
            {
                heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() - 7);
            }
            stampanje(heroj1, heroj2);
        }

        /**
         * Ova metoda proverava kriticnu sansu i ispravlja trenutno zdravlje heroja
         *
         * @param heroj1
         * @param heroj2
         */
        public void proveraKriticneSanseHeroj2(heroj.Heroj heroj1, heroj.Heroj heroj2, int carolija)
        {

            if (carolija == 1)
            {
                if (heroj1.getTrenutnoZdravlje() >= osnovniUdarac.getMinNanesenaSteta())
                {
                    heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() - osnovniUdarac.getMinNanesenaSteta());
                }
                else if (heroj1.getTrenutnoZdravlje() >= osnovniUdarac.getMaxNanesenaSteta())
                {
                    heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() - osnovniUdarac.getMaxNanesenaSteta());
                }
            }
            else if (carolija == 2)
            {
                if (heroj1.getTrenutnoZdravlje() >= magicniUdarac.getMinNanesenaSteta())
                {
                    heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() - magicniUdarac.getMinNanesenaSteta());
                }
                else if (heroj1.getTrenutnoZdravlje() >= magicniUdarac.getMaxNanesenaSteta())
                {
                    heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() - magicniUdarac.getMaxNanesenaSteta());
                }
            }
            else if (carolija == 3)
            {
                if (heroj1.getTrenutnoZdravlje() >= snazniUdarac.getMinNanesenaSteta())
                {
                    heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() - snazniUdarac.getMinNanesenaSteta());
                }
                else if (heroj1.getTrenutnoZdravlje() >= snazniUdarac.getMaxNanesenaSteta())
                {
                    heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() - snazniUdarac.getMaxNanesenaSteta());
                }
            }
            else if (carolija == 4)
            {
                if (heroj1.getTrenutnoZdravlje() >= brzUdarac.getMinNanesenaSteta())
                {
                    heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() - brzUdarac.getMinNanesenaSteta());
                }
                else if (heroj1.getTrenutnoZdravlje() >= brzUdarac.getMaxNanesenaSteta())
                {
                    heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() - brzUdarac.getMaxNanesenaSteta());
                }
            }
            if (heroj1.getTrenutnoZdravlje() >= heroj1.getMaxSteta())
            {
                heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() - heroj1.getMaxSteta());
            }
            else if (heroj1.getTrenutnoZdravlje() >= heroj1.getMinSteta())
            {
                heroj1.setTrenutnoZdravlje(heroj1.getTrenutnoZdravlje() - heroj1.getMinSteta());
            }
            heroj2.setTrenutnoIskustvo(heroj2.getTrenutnoIskustvo() + 4);
            nivo(heroj2);
            if (heroj2.getTrenutniNivo() == 2 || heroj2.getTrenutniNivo() == 3)
            {
                heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() + 7);
            }
            if (heroj2.getTrenutniNivo() == 4 || heroj2.getTrenutniNivo() == 5)
            {
                heroj2.setTrenutnoZdravlje(heroj2.getTrenutnoZdravlje() - 7);
            }
            stampanje(heroj1, heroj2);
        }

        /**
         * Ova metoda dodaje carolije igracima
         *
         * @param heroj
         */
        public void carolije(heroj.Heroj heroj)
        {

            if (heroj == carobnjak)
            {
                carobnjak.setCarolija1(osnovniUdarac);
                carobnjak.setCarolija2(magicniUdarac);
                carobnjak.setCarolija3(brzUdarac);
            }
            else if (heroj == svestenik)
            {
                svestenik.setCarolija1(osnovniUdarac);
                svestenik.setCarolija2(brzUdarac);
                svestenik.setCarolija3(snazniUdarac);
            }
            else
            {
                ratnik.setCarolija1(osnovniUdarac);
                ratnik.setCarolija2(snazniUdarac);
                ratnik.setCarolija3(magicniUdarac);
            }
        }


        /**
        * Ova metoda u zavisnosti od iskstva odredjuje nivo
        *
        * @param heroj
        */
        public void nivo(heroj.Heroj heroj)
        {
            if (heroj.getTrenutnoIskustvo() > 10 && heroj.getTrenutnoIskustvo() <= 20)
            {
                heroj.setTrenutniNivo(2);
            }
            else if (heroj.getTrenutnoIskustvo() > 20 && heroj.getTrenutnoIskustvo() < 30)
            {
                heroj.setTrenutniNivo(3);
            }
            else if (heroj.getTrenutnoIskustvo() >= 30 && heroj.getTrenutnoIskustvo() < 40)
            {
                heroj.setTrenutniNivo(4);
            }
            else if (heroj.getTrenutnoIskustvo() >= 40 && heroj.getTrenutnoIskustvo() < 50)
            {
                heroj.setTrenutniNivo(5);
            }
        }

        /**
        * Ova metoda vraca naziv arene koju je igrac odabrao
        *
        * @return (osnovnaArena ili ledenaArena ili arenaUbica)
        * default- osnovnaArena
        */
        public string nazivArene()
        {

            string borbenaArena = Arena.odabranaArena;
            switch (borbenaArena)
            {
                case "1":
                    arenaZaIgrace = 1;
                    return osnovnaArena.getImeArene();
                case "2":
                    arenaZaIgrace = 2;
                    return ledenaArena.getImeArene();
                case "3":
                    arenaZaIgrace = 3;
                    return arenaUbica.getImeArene();
                default:
                    arenaZaIgrace = 1;
                    return osnovnaArena.getImeArene();
            }
        }
        public void proveraHeroja2(heroj.Heroj heroj, int carolija)
        {
            if (protivnik1 == carobnjak.getNazivHeroja())
            {
                proveraKriticneSanseHeroj2(carobnjak, heroj, carolija);
            }
            else if (protivnik1 == svestenik.getNazivHeroja())
            {
                proveraKriticneSanseHeroj2(svestenik, heroj, carolija);
            }
            else if (protivnik1 == ratnik.getNazivHeroja())
            {
                proveraKriticneSanseHeroj2(ratnik, heroj, carolija);
            }
        }
        public void odabirCarolijeHeroja1(heroj.Heroj heroj)
        {

            if (arenaZaIgrace == 2)
            {
                heroj.setTrenutnoZdravlje(heroj.getTrenutnoZdravlje() - 4);
            }
            else if (arenaZaIgrace == 3)
            {
                heroj.setTrenutnoZdravlje(heroj.getTrenutnoZdravlje() - 2);
            }

            Thread.Sleep(6000);
            string spell = Igra.odabranaCarolijaIgrac1;
            switch (spell)
            {
                case "1":
                    proveraHeroja1(heroj, 1);
                    break;
                case "2":
                    if (heroj.getNazivHeroja() == svestenik.getNazivHeroja())
                    {
                        //You lost the move, you don't have that spell!
                        break;
                    }
                    else
                    {
                        if (heroj.getTrenutnaSnaga() >= 4)
                        {
                            heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);
                        }
                        proveraHeroja1(heroj, 2);
                        break;
                    }

                case "3":
                    if (heroj.getNazivHeroja() == carobnjak.getNazivHeroja())
                    {
                        //You lost the move, you don't have that spell!
                        break;
                    }
                    else
                    {
                        if (heroj.getTrenutnaSnaga() >= 4)
                        {
                            heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);
                        }
                        proveraHeroja1(heroj, 3);
                        break;
                    }
                case "4":
                    if (heroj.getNazivHeroja() == ratnik.getNazivHeroja())
                    {
                        //You lost the move, you don't have that spell!
                        break;
                    }
                    else if (heroj.getTrenutnaSnaga() >= 4)
                    {
                        heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);
                    }
                    proveraHeroja1(heroj, 4);
                    break;
                case "5":
                    if (heroj.getTrenutnaSnaga() >= 4)
                    {
                        heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);

                        if (heroj.getTrenutnoZdravlje() < 25 && heroj.getTrenutnoZdravlje() > 0)
                        {
                            heroj.setTrenutnoZdravlje(heroj.getTrenutnoZdravlje() + 5);
                        }
                    }
                    else
                    {
                        //  Console.WriteLine("Sorry...");
                    }
                    break;
                default:
                    proveraHeroja1(heroj, 5);
                    break;
            }
        }

        /**
        * Ova metoda stampa trenutne rezultate igraca(nivo, trenutno iskustvo, trenutno zdravlje, trenutnu snagu, carolije)
        *
        * @param heroj1 -borac 1
        * @param heroj2 -borac 2
        */
        public void stampanje(heroj.Heroj heroj1, heroj.Heroj heroj2)
        {
            nivo1 = heroj1.getTrenutniNivo().ToString();
            nivo2 = heroj2.getTrenutniNivo().ToString();
            snaga1 = heroj1.getTrenutnaSnaga().ToString();
            snaga2 = heroj2.getTrenutnaSnaga().ToString();
            zdravlje1 = heroj1.getTrenutnoZdravlje().ToString();
            zdravlje2 = heroj2.getTrenutnoZdravlje().ToString();
            iskustvo1 = heroj1.getTrenutnoIskustvo().ToString();
            iskustvo2 = heroj2.getTrenutnoIskustvo().ToString();
            eksponat11 = heroj1.getCarolija1().getNazivCarolije().ToString();
            eksponat12 = heroj1.getCarolija2().getNazivCarolije().ToString();
            eksponat13 = heroj1.getCarolija3().getNazivCarolije().ToString();
            eksponat21 = heroj2.getCarolija1().getNazivCarolije().ToString();
            eksponat22 = heroj2.getCarolija2().getNazivCarolije().ToString();
            eksponat23 = heroj2.getCarolija3().getNazivCarolije().ToString();
        }

        /**
         * Ova metoda vraca naziv heroja
         *
         * @return (ratnik ili svestenik ili carobnjak)
         * default- svestenik
         */
         public string nazivPrvogIgraca()
        {

            string izabraniPrviIgrac = Heroj.odabraniHeroj;
            switch (izabraniPrviIgrac)
            {
                case "1":
                    protivnik1 = svestenik.getNazivHeroja();
                    return protivnik1;
                case "2":
                    protivnik1 = ratnik.getNazivHeroja();
                    return protivnik1;
                case "3":
                    protivnik1 = carobnjak.getNazivHeroja();
                    return protivnik1;
                default:
                    return svestenik.getNazivHeroja();
            }
        }


        public void odabirCarolijeHeroja2(heroj.Heroj heroj)
        {

            if (arenaZaIgrace == 2)
            {
                heroj.setTrenutnoZdravlje(heroj.getTrenutnoZdravlje() - 4);
            }
            else if (arenaZaIgrace == 3)
            {
                heroj.setTrenutnoZdravlje(heroj.getTrenutnoZdravlje() - 2);
            }
            if (nacinBorbeUAreni() == "Osnovni")
            {
                Thread.Sleep(6000);
                Random rand = new Random();
                int randNumber = rand.Next(10);
                switch (randNumber)
                {
                    case 1:
                        proveraHeroja2(heroj, 1);
                        break;
                    case 2:
                        if (heroj.getNazivHeroja() == svestenik.getNazivHeroja())
                        {
                            break;
                        }
                        else
                        {
                            if (heroj.getTrenutnaSnaga() >= 4)
                            {
                                heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);
                            }
                            proveraHeroja2(heroj, 2);
                            break;
                        }
                    case 3:
                        if (heroj.getNazivHeroja() == carobnjak.getNazivHeroja())
                        {
                            break;
                        }
                        else
                        {
                            if (heroj.getTrenutnaSnaga() >= 4)
                            {
                                heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);
                            }
                            proveraHeroja2(heroj, 3);
                            break;
                        }
                    case 4:
                        if (heroj.getNazivHeroja() == ratnik.getNazivHeroja())
                        {
                            break;
                        }
                        else
                        {
                            if (heroj.getTrenutnaSnaga() >= 4)
                            {
                                heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);
                            }
                            proveraHeroja2(heroj, 4);
                            break;
                        }
                    case 5:
                        if (heroj.getTrenutnaSnaga() >= 4)
                        {
                            heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);
                            if (heroj.getTrenutnoZdravlje() < 25 && heroj.getTrenutnoZdravlje() > 0)
                            {
                                heroj.setTrenutnoZdravlje(heroj.getTrenutnoZdravlje() + 5);
                            }
                        }
                        else
                        {
                            //  Console.WriteLine("Sorry...");
                        }
                        break;
                    default:
                        proveraHeroja2(heroj, 1);
                        break;
                }
            }
            else
            {
                Thread.Sleep(6000);
                string wantSpell = Igra.odabranaCarolijaIgrac2;
                switch (wantSpell)
                {
                    case "1":
                        proveraHeroja2(heroj, 1);
                        break;
                    case "2":
                        if (heroj.getNazivHeroja() == svestenik.getNazivHeroja())
                        {
                            //You lost the move, you don't have that spell!
                            break;
                        }
                        else
                        {
                            if (heroj.getTrenutnaSnaga() >= 4)
                            {
                                heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);
                            }
                            proveraHeroja2(heroj, 2);
                            break;
                        }
                    case "3":
                        if (heroj.getNazivHeroja() == carobnjak.getNazivHeroja())
                        {
                            //You lost the move, you don't have that spell!
                            break;
                        }
                        else
                        {
                            if (heroj.getTrenutnaSnaga() >= 4)
                            {
                                heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);
                            }
                            proveraHeroja2(heroj, 3);
                            break;
                        }
                    case "4":
                        if (heroj.getNazivHeroja() == ratnik.getNazivHeroja())
                        {
                            //You lost the move, you don't have that spell!
                            break;
                        }
                        else
                        {
                            if (heroj.getTrenutnaSnaga() >= 4)
                            {
                                heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);
                            }
                            proveraHeroja2(heroj, 4);
                            break;
                        }
                    case "5":
                        if (heroj.getTrenutnaSnaga() >= 4)
                        {
                            heroj.setTrenutnaSnaga(heroj.getTrenutnaSnaga() - 4);
                            if (heroj.getTrenutnoZdravlje() < 25 && heroj.getTrenutnoZdravlje() > 0)
                            {
                                heroj.setTrenutnoZdravlje(heroj.getTrenutnoZdravlje() + 5);
                            }
                        }
                        else
                        {
                            //  Console.WriteLine("Sorry...");
                        }
                        break;
                    default:
                        proveraHeroja2(heroj, 1);
                        break;
                }
            }
        }

        /**
         * Ova metoda vraca ime protivnika (random odabir)
         *
         * @return (svestenik ili carobnjak ili ratnik)
         */
        public  string nazivDrugogIgraca()
        {
            for (; ; )
            {
                Random rand = new Random();
                int drugiRandomOdabraniIgrac = rand.Next(3);
                if ((protivnik1 == "Svestenik" || protivnik1 == "Ratnik") && drugiRandomOdabraniIgrac == 1)
                {
                    protivnik2 = carobnjak.getNazivHeroja();
                    return protivnik2;
                }
                else if ((protivnik1 == "Carobnjak" || protivnik1 == "Ratnik") && drugiRandomOdabraniIgrac == 2)
                {
                    protivnik2 = svestenik.getNazivHeroja();
                    return protivnik2;
                }
                else if ((protivnik1 == "Carobnjak" || protivnik1 == "Svestenik") && drugiRandomOdabraniIgrac == 3)
                {
                    protivnik2 = ratnik.getNazivHeroja();
                    return protivnik2;
                }
            }
        }

        /**
         * Ova metoda vraca random broj
         *
         * @return (broj od 0 do 40)
         */
        public int randomKriticnaSansa()
        {
        
            int max = 40;
            int min = 1;
            int kriticnaSansa = min + randomIzabranBroj.Next(max);
            return kriticnaSansa;
        }

       public void potezPrvogIgraca()
        {
            try
            {
                Thread.Sleep(2000);
                prviBorac.WaitOne();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            if (nazivPrvogIgraca() == carobnjak.getNazivHeroja())
            {
                try
                {
                    odabirCarolijeHeroja1(carobnjak);
                }

                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }
            else if (nazivPrvogIgraca() == svestenik.getNazivHeroja())
            {
                try
                {
                    odabirCarolijeHeroja1(svestenik);
                }

                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            else
            {
                try
                {
                    odabirCarolijeHeroja1(ratnik);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }
            drugiBorac.Release();
        }
        /**
     *
     * This method checks and returns the winner
     *
     * @return the winner
     */
        public string rezultat()
        {
            string result = "";
            if (protivnik1 == "Svestenik")
            {
                if (protivnik2 == "Ratnik")
                {
                    if (svestenik.getTrenutnoZdravlje() > ratnik.getTrenutnoZdravlje())
                    {
                        result = "Pobednik je Svestenik";
                    }
                    else
                    {
                        result = "Pobednik je Ratnik";
                    }
                }
                else if ((protivnik2 == "Carobnjak"))
                {
                    if (svestenik.getTrenutnoZdravlje() > carobnjak.getTrenutnoZdravlje())
                    {
                        result = "Pobednik je Svestenik";
                    }
                    else
                    {
                        result = "Pobednik je Carobnjak";
                    }
                }
            }
            else if (protivnik1 == "Ratnik")
            {
                if (protivnik2 == "Svestenik")
                {
                    if (ratnik.getTrenutnoZdravlje() > svestenik.getTrenutnoZdravlje())
                    {
                        result = "Pobednik je Ratnik";
                    }
                    else
                    {
                        result = "Pobednik je Svestenik";
                    }
                }
                else if (protivnik2 == "Carobnjak")
                {
                    if (ratnik.getTrenutnoZdravlje() > carobnjak.getTrenutnoZdravlje())
                    {
                        result = "Pobednik je Ratnik";
                    }
                    else
                    {
                        result = "Pobednik je Carobnjak";
                    }
                }
            }
            else
            {
                if (protivnik2 == "Svestenik")
                {
                    if (svestenik.getTrenutnoZdravlje() > svestenik.getTrenutnoZdravlje())
                    {
                        result = "Pobednik je Carobnjak";
                    }
                    else
                    {
                        result = "Pobednik je Svestenik";
                    }
                }
                else if (protivnik1 == "Ratnik")
                {
                    if (svestenik.getTrenutnoZdravlje() > ratnik.getTrenutnoZdravlje())
                    {
                        result = "Pobednik je Carobnjak";
                    }
                    else
                    {
                        result = "Pobednik je Ratnik";
                    }
                }
            }
            return result;
        }
        public void potezDrugogIgraca()
        {
            try
            {
                   Thread.Sleep(2000);
                drugiBorac.WaitOne();
                Thread.Sleep(2000);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            if (nazivDrugogIgraca() == carobnjak.getNazivHeroja())
            {
                odabirCarolijeHeroja2(carobnjak);
            }
            else if (nazivDrugogIgraca() == svestenik.getNazivHeroja())
            {
                odabirCarolijeHeroja2(svestenik);
            }
            else
            {
                odabirCarolijeHeroja2(ratnik);
            }
            prviBorac.Release();
        }
    }
}

