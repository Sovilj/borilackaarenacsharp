﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Unity;

namespace borilackaArena.borba
{

    public class Heroj1 
    {

        Borba borba;

        public Heroj1(Borba b)
        {
            borba = b;
        }

        public void run()
        {

            int kriticnaSansa1 = borba.randomKriticnaSansa();

            if (borba.nazivPrvogIgraca() == "Carobnjak")
            {
                if (borba.nazivDrugogIgraca() == "Svestenik")
                {
                    if (kriticnaSansa1 >= 30)
                    {
                        while (borba.getCarobnjak().getTrenutnaSnaga() >= 0 && borba.getSvestenik().getTrenutnaSnaga() >= 0 && borba.getCarobnjak().getTrenutnoZdravlje() > 0 && borba.getSvestenik().getTrenutnoZdravlje() > 0)
                        {
                            borba.potezPrvogIgraca();
                        }
                    }
                    else
                    {
                        while (borba.getCarobnjak().getTrenutnaSnaga() >= 0 && borba.getSvestenik().getTrenutnaSnaga() >= 0 && borba.getCarobnjak().getTrenutnoZdravlje() > 0 && borba.getSvestenik().getTrenutnoZdravlje() > 0)
                        {
                            borba.potezPrvogIgraca();
                        }
                    }
                }
                else if (borba.nazivDrugogIgraca() == "Ratnik")
                {
                    if (kriticnaSansa1 >= 30)
                    {
                        while (borba.getCarobnjak().getTrenutnaSnaga() >= 0 && borba.getRatnik().getTrenutnaSnaga() >= 0 && borba.getCarobnjak().getTrenutnoZdravlje() > 0 && borba.getRatnik().getTrenutnoZdravlje() > 0)
                        {
                            borba.potezPrvogIgraca();
                        }
                    }
                    else
                    {
                        while (borba.getCarobnjak().getTrenutnaSnaga() >= 0 && borba.getRatnik().getTrenutnaSnaga() >= 0 && borba.getCarobnjak().getTrenutnoZdravlje() > 0 && borba.getRatnik().getTrenutnoZdravlje() > 0)
                        {
                            borba.potezPrvogIgraca();
                        }
                    }
                }
            }
            else if (borba.nazivPrvogIgraca() == "Svestenik")
            {
                if (borba.nazivDrugogIgraca() == "Carobnjak")
                {
                    if (kriticnaSansa1 >= 30)
                    {
                        while (borba.getSvestenik().getTrenutnaSnaga() >= 0 && borba.getCarobnjak().getTrenutnaSnaga() >= 0 && borba.getSvestenik().getTrenutnoZdravlje() > 0 && borba.getCarobnjak().getTrenutnoZdravlje() > 0)
                        {
                            borba.potezPrvogIgraca();
                        }
                    }
                    else
                    {
                        while (borba.getSvestenik().getTrenutnaSnaga() >= 0 && borba.getCarobnjak().getTrenutnaSnaga() >= 0 && borba.getSvestenik().getTrenutnoZdravlje() > 0 && borba.getCarobnjak().getTrenutnoZdravlje() > 0)
                        {
                            borba.potezPrvogIgraca();
                        }
                    }
                }
                else if (borba.nazivPrvogIgraca() == "Ratnik")
                {
                    if (kriticnaSansa1 >= 30)
                    {
                        while (borba.getSvestenik().getTrenutnaSnaga() >= 0 && borba.getRatnik().getTrenutnaSnaga() >= 0 && borba.getSvestenik().getTrenutnoZdravlje() > 0 && borba.getRatnik().getTrenutnoZdravlje() > 0)
                        {
                            borba.potezPrvogIgraca();
                        }
                    }
                    else
                    {
                        while (borba.getSvestenik().getTrenutnaSnaga() >= 0 && borba.getRatnik().getTrenutnaSnaga() >= 0 && borba.getSvestenik().getTrenutnoZdravlje() > 0 && borba.getRatnik().getTrenutnoZdravlje() > 0)
                        {
                            borba.potezPrvogIgraca();
                        }
                    }
                }
            }
            else
            {
                if (borba.nazivDrugogIgraca() == "Carobnjak")
                {
                    if (kriticnaSansa1 >= 30)
                    {
                        while (borba.getRatnik().getTrenutnaSnaga() >= 0 && borba.getCarobnjak().getTrenutnaSnaga() >= 0 && borba.getRatnik().getTrenutnoZdravlje() > 0 && borba.getCarobnjak().getTrenutnoZdravlje() > 0)
                        {
                            borba.potezPrvogIgraca();
                        }
                    }
                    else
                    {
                        while (borba.getRatnik().getTrenutnaSnaga() >= 0 && borba.getCarobnjak().getTrenutnaSnaga() >= 0 && borba.getRatnik().getTrenutnoZdravlje() > 0 && borba.getCarobnjak().getTrenutnoZdravlje() > 0)
                        {
                            borba.potezPrvogIgraca();
                        }
                    }
                }
                else if (borba.nazivDrugogIgraca() == "Svestenik")
                {
                    if (kriticnaSansa1 >= 30)
                    {
                        while (borba.getRatnik().getTrenutnaSnaga() >= 0 && borba.getSvestenik().getTrenutnaSnaga() >= 0 && borba.getRatnik().getTrenutnoZdravlje() > 0 && borba.getSvestenik().getTrenutnoZdravlje() > 0)
                        {
                            borba.potezPrvogIgraca();
                        }
                    }
                    else
                    {
                        while (borba.getRatnik().getTrenutnaSnaga() >= 0 && borba.getSvestenik().getTrenutnaSnaga() >= 0 && borba.getRatnik().getTrenutnoZdravlje() > 0 && borba.getSvestenik().getTrenutnoZdravlje() > 0)
                        {
                            borba.potezPrvogIgraca();
                        }
                    }
                }
            }
        }
    }

}
