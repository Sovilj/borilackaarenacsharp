﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Unity;

namespace borilackaArena.borba
{

    public class Heroj2
    {

        Borba borba;

        public Heroj2(Borba b)
        {
            borba = b;
        }

        public void run()
        {

            int kriticnaSansa2 = borba.randomKriticnaSansa();
            if (borba.nazivDrugogIgraca() == "Carobnjak")
            {
                if (borba.nazivPrvogIgraca() == "Svestenik")
                {
                    if (kriticnaSansa2 >= 30)
                    {
                        while (borba.getCarobnjak().getTrenutnaSnaga() >= 0 && borba.getSvestenik().getTrenutnaSnaga() >= 0 && borba.getCarobnjak().getTrenutnoZdravlje() > 0 && borba.getSvestenik().getTrenutnoZdravlje() > 0)
                        {
                            try
                            {
                                borba.potezDrugogIgraca();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        }
                    }
                    else
                    {
                        while (borba.getCarobnjak().getTrenutnaSnaga() >= 0 && borba.getSvestenik().getTrenutnaSnaga() >= 0 && borba.getCarobnjak().getTrenutnoZdravlje() > 0 && borba.getSvestenik().getTrenutnoZdravlje() > 0)
                        {
                            try
                            {
                                borba.potezDrugogIgraca();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        }
                    }
                }
                else if (borba.nazivPrvogIgraca() == "Ratnik")
                {
                    if (kriticnaSansa2 >= 30)
                    {
                        while (borba.getCarobnjak().getTrenutnaSnaga() >= 0 && borba.getRatnik().getTrenutnaSnaga() >= 0 && borba.getCarobnjak().getTrenutnoZdravlje() > 0 && borba.getRatnik().getTrenutnoZdravlje() > 0)
                        {
                            try
                            {
                                borba.potezDrugogIgraca();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        }
                    }
                    else
                    {
                        while (borba.getCarobnjak().getTrenutnaSnaga() >= 0 && borba.getRatnik().getTrenutnaSnaga() >= 0 && borba.getCarobnjak().getTrenutnoZdravlje() > 0 && borba.getRatnik().getTrenutnoZdravlje() > 0)
                        {
                            try
                            {
                                borba.potezDrugogIgraca();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        }
                    }
                }
            }
            else if (borba.nazivDrugogIgraca() == "Svestenik")
            {
                if (borba.nazivPrvogIgraca() == "Carobnjak")
                {
                    if (kriticnaSansa2 >= 30)
                    {
                        while (borba.getSvestenik().getTrenutnaSnaga() >= 0 && borba.getCarobnjak().getTrenutnaSnaga() >= 0 && borba.getSvestenik().getTrenutnoZdravlje() > 0 && borba.getCarobnjak().getTrenutnoZdravlje() > 0)
                        {
                            try
                            {
                                borba.potezDrugogIgraca();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        }
                    }
                    else
                    {
                        while (borba.getSvestenik().getTrenutnaSnaga() >= 0 && borba.getCarobnjak().getTrenutnaSnaga() >= 0 && borba.getSvestenik().getTrenutnoZdravlje() > 0 && borba.getCarobnjak().getTrenutnoZdravlje() > 0)
                        {
                            try
                            {
                                borba.potezDrugogIgraca();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        }
                    }
                }
                else if (borba.nazivPrvogIgraca() == "Ratnik")
                {
                    if (kriticnaSansa2 >= 30)
                    {
                        while (borba.getSvestenik().getTrenutnaSnaga() >= 0 && borba.getRatnik().getTrenutnaSnaga() >= 0 && borba.getSvestenik().getTrenutnoZdravlje() > 0 && borba.getRatnik().getTrenutnoZdravlje() > 0)
                        {
                            try
                            {
                                borba.potezDrugogIgraca();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        }
                    }
                    else
                    {
                        while (borba.getSvestenik().getTrenutnaSnaga() >= 0 && borba.getRatnik().getTrenutnaSnaga() >= 0 && borba.getSvestenik().getTrenutnoZdravlje() > 0 && borba.getRatnik().getTrenutnoZdravlje() > 0)
                        {
                            try
                            {
                                borba.potezDrugogIgraca();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        }
                    }
                }
            }
            else
            {
                if (borba.nazivPrvogIgraca() == "Carobnjak")
                {
                    if (kriticnaSansa2 >= 30)
                    {
                        while (borba.getRatnik().getTrenutnaSnaga() >= 0 && borba.getCarobnjak().getTrenutnaSnaga() >= 0 && borba.getRatnik().getTrenutnoZdravlje() > 0 && borba.getCarobnjak().getTrenutnoZdravlje() > 0)
                        {
                            try
                            {
                                borba.potezDrugogIgraca();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        }
                    }
                    else
                    {
                        while (borba.getRatnik().getTrenutnaSnaga() >= 0 && borba.getCarobnjak().getTrenutnaSnaga() >= 0 && borba.getRatnik().getTrenutnoZdravlje() > 0 && borba.getCarobnjak().getTrenutnoZdravlje() > 0)
                        {
                            try
                            {
                                borba.potezDrugogIgraca();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        }
                    }
                }
                else if (borba.nazivPrvogIgraca() == "Svestenik")
                {
                    if (kriticnaSansa2 >= 30)
                    {
                        while (borba.getRatnik().getTrenutnaSnaga() >= 0 && borba.getSvestenik().getTrenutnaSnaga() >= 0 && borba.getRatnik().getTrenutnoZdravlje() > 0 && borba.getSvestenik().getTrenutnoZdravlje() > 0)
                        {

                            try
                            {
                                borba.potezDrugogIgraca();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        }
                    }
                    else
                    {
                        while (borba.getRatnik().getTrenutnaSnaga() >= 0 && borba.getSvestenik().getTrenutnaSnaga() >= 0 && borba.getRatnik().getTrenutnoZdravlje() > 0 && borba.getSvestenik().getTrenutnoZdravlje() > 0)
                        {

                            try
                            {
                                borba.potezDrugogIgraca();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        }
                    }
                }
            }
        }
    }
}
