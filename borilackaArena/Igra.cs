﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using borilackaArena;
using borilackaArena.borba;
using System.Threading;
using System.Data.SQLite;
using java.sql;

namespace borilackaArena
{
    public partial class Igra : Form
    {
        public static string nivo1, nivo2, snaga1, snaga2, zdravlje1, zdravlje2, iskustvo1, iskustvo2, eksponat11, eksponat12, eksponat13, eksponat21, eksponat22, eksponat23;
        public static string odabranaCarolijaIgrac1 = "0", odabranaCarolijaIgrac2 = "0";
        public static Borba borba = new Borba();
        public static Heroj1 heroj1 = new Heroj1(borba);
        public static Heroj2 heroj2 = new Heroj2(borba);
        Thread nit1 = new Thread(new ThreadStart(heroj1.run));
        Thread nit2 = new Thread(new ThreadStart(heroj2.run));

        public Igra()
        {
            InitializeComponent();
        }

        private void sledecaStrana(object sender, EventArgs e)
        {

            try
            {
                //zaustavljanje niti
                nit1.Abort();
                nit2.Abort();

                //konekcija
                SQLiteConnection sqliteKonekcija = Konekcija();

                //kreiranje tabele
                SQLiteCommand sqlite_cmd;

                string sql = "CREATE TABLE IF NOT EXISTS bitke" +
                "(id integer PRIMARY KEY AUTOINCREMENT," +
                "nazivArene text NOT NULL, " +
                "heroj1 text NOT NULL," +
                "heroj2 text NOT NULL," +
                "nacinBorbe text NOT NULL ," +
                "rezultat text NOT NULL)";

                sqlite_cmd = sqliteKonekcija.CreateCommand();
                sqlite_cmd.CommandText = sql;
                sqlite_cmd.ExecuteNonQuery();

                //unosenje podataka u tabelu
                sqlite_cmd.CommandText = "INSERT INTO bitke(nazivArene, heroj1, heroj2, nacinBorbe, rezultat) " +
                    "VALUES(@borbenaArena, @igrac1, @igrac2, @nacinBorbeUAreni, @rezultat)";
                sqlite_cmd.Parameters.AddWithValue("@borbenaArena", borbenaArena.Text);
                sqlite_cmd.Parameters.AddWithValue("@igrac1", igrac1.Text);
                sqlite_cmd.Parameters.AddWithValue("@igrac2", igrac2.Text);
                sqlite_cmd.Parameters.AddWithValue("@nacinBorbeUAreni", vrstaBorbe.Text);
                sqlite_cmd.Parameters.AddWithValue("@rezultat", borba.rezultat());
                sqlite_cmd.Prepare();
                sqlite_cmd.ExecuteNonQuery();

                //stampanje
                string stm = "SELECT * FROM bitke";

                var cmd = new SQLiteCommand(stm, sqliteKonekcija);
                SQLiteDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Console.WriteLine($"{rdr.GetInt32(0)} {rdr.GetString(1)} {rdr.GetString(2)} {rdr.GetString(3)} {rdr.GetString(4)} {rdr.GetString(5)}");
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Kraj kraj = new Kraj();
            kraj.Show();
            this.Hide();
        }

          static SQLiteConnection Konekcija()
        {
            SQLiteConnection sqlite_conn;
            sqlite_conn = new SQLiteConnection("DataSource=C:/borilackaArenaC#/db/Borba.db");
            try
            {
                sqlite_conn.Open();
                Console.WriteLine("uspesno");
            }
            catch (SQLiteException e)
            {
                Console.WriteLine(e.Message);
            }
            return sqlite_conn;
        }
        private void zapocni(object sender, EventArgs e)
        {
            string arenaName = borba.nazivArene();
            string nacinBorbeUAreni = borba.nacinBorbeUAreni();
            borba.prikazRezultata();
            nivoHeroj1.Text = Borba.nivo1;
            nivoHeroj2.Text = Borba.nivo2;
            iskustvoHeroj1.Text = Borba.iskustvo1;
            iskustvoHeroj2.Text  = Borba.iskustvo2;
            zdravljeHeroj1.Text=Borba.zdravlje1;
            zdravljeHeroj2.Text=Borba.zdravlje2;
            snagaHeroj1.Text=Borba.snaga1;
            snagaHeroj2.Text=Borba.snaga2;
            carolija1Heroj1.Text = Borba.eksponat11;
            carolija2Heroj1.Text = Borba.eksponat12;
            carolija3Heroj1.Text = Borba.eksponat13;
            carolija1Heroj2.Text = Borba.eksponat21;
            carolija2Heroj2.Text = Borba.eksponat22;
            carolija3Heroj2.Text = Borba.eksponat23;
            igrac1.Text = borba.nazivPrvogIgraca();
            igrac2.Text = borba.nazivDrugogIgraca();
            borbenaArena.Text = arenaName;
            vrstaBorbe.Text = nacinBorbeUAreni;

        }

        private void zapocniIgru(object sender, EventArgs e)
        {
            borba.prikazRezultata();
            nivoHeroj1.Text = Borba.nivo1;
            nivoHeroj1.Text = Borba.nivo2;
            iskustvoHeroj1.Text = Borba.iskustvo1;
            iskustvoHeroj2.Text = Borba.iskustvo2;
            zdravljeHeroj1.Text = Borba.zdravlje1;
            zdravljeHeroj2.Text = Borba.zdravlje2;
            snagaHeroj1.Text = Borba.snaga1;
            snagaHeroj2.Text = Borba.snaga2;
            carolija1Heroj1.Text = Borba.eksponat11;
            carolija2Heroj1.Text = Borba.eksponat12;
            carolija3Heroj1.Text = Borba.eksponat13;
            carolija1Heroj2.Text = Borba.eksponat21;
            carolija2Heroj2.Text = Borba.eksponat22;
            carolija3Heroj2.Text = Borba.eksponat23;
            odabranaCarolijaIgrac1 = izabranaCarolija1.Text;
            odabranaCarolijaIgrac2 = izabranaCarolija2.Text;
            nit1.Start();
            nit2.Start();
            borba.prikazRezultata();
            nivoHeroj1.Text = Borba.nivo1;
            nivoHeroj1.Text = Borba.nivo2;
            iskustvoHeroj1.Text = Borba.iskustvo1;
            iskustvoHeroj2.Text = Borba.iskustvo2;
            zdravljeHeroj1.Text = Borba.zdravlje1;
            zdravljeHeroj2.Text = Borba.zdravlje2;
            snagaHeroj1.Text = Borba.snaga1;
            snagaHeroj2.Text = Borba.snaga2;
            carolija1Heroj1.Text = Borba.eksponat11;
            carolija2Heroj1.Text = Borba.eksponat12;
            carolija3Heroj1.Text = Borba.eksponat13;
            carolija1Heroj2.Text = Borba.eksponat21;
            carolija2Heroj2.Text = Borba.eksponat22;
            carolija3Heroj2.Text = Borba.eksponat23;
            odabranaCarolijaIgrac1 = izabranaCarolija1.Text;
            odabranaCarolijaIgrac2 = izabranaCarolija2.Text;

        }

        private void trenutanRezultat(object sender, EventArgs e)
        {
            borba.prikazRezultata();
            nivoHeroj1.Text = Borba.nivo1;
            nivoHeroj1.Text = Borba.nivo2;
            iskustvoHeroj1.Text = Borba.iskustvo1;
            iskustvoHeroj2.Text = Borba.iskustvo2;
            zdravljeHeroj1.Text = Borba.zdravlje1;
            zdravljeHeroj2.Text = Borba.zdravlje2;
            snagaHeroj1.Text = Borba.snaga1;
            snagaHeroj2.Text = Borba.snaga2;
            carolija1Heroj1.Text = Borba.eksponat11;
            carolija2Heroj1.Text = Borba.eksponat12;
            carolija3Heroj1.Text = Borba.eksponat13;
            carolija1Heroj2.Text = Borba.eksponat21;
            carolija2Heroj2.Text = Borba.eksponat22;
            carolija3Heroj2.Text = Borba.eksponat23;
            odabranaCarolijaIgrac1 = izabranaCarolija1.Text;
            odabranaCarolijaIgrac2 = izabranaCarolija2.Text;
        }     
    }
}
